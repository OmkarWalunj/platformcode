import java.util.*;
class Solution {
    boolean hasArrayTwoCandidates(int arr[], int n, int x) {
        
        
        LinkedHashSet<Integer> lh=new LinkedHashSet<Integer>();
        
        for(int i=0;i<n;i++){
            if(lh.contains(arr[i])){
                return true;
            }else{
                int num=x-arr[i];
                lh.add(num);
            }
        }
        return false;
    }
}


class client{
	
	public static void main(String [] args){

		System.out.println("Enter Size of Array: ");
		
		Scanner sc=new Scanner(System.in);
		int size=sc.nextInt();
		int arr[]= new int[size];
		
		System.out.println("Enter Array elements:");
		for(int i=0;i<arr.length;i++){

			arr[i]=sc.nextInt();
		}
		
		System.out.println("Enter Sum :");
		int x=sc.nextInt();
		Solution obj=new Solution();
		boolean val=obj.hasArrayTwoCandidates(arr,arr.length,x);

		System.out.println(val);
	}
}
