import java.util.*;
class Solution {
    // Function to return the position of the first repeating element.
    public static int firstRepeated(int[] arr, int n) {
        // Your code here
        int min=-1;
        
	LinkedHashSet<Integer> lh=new LinkedHashSet<Integer>();
        
        for(int i=n-1;i>=0;i--){
            
            if(lh.contains(arr[i])){
              min=i+1;
            }else{
                lh.add(arr[i]);
            }
        }
        if(min==-1){
            return -1;
        }else{
            return min;
        }
    }
}


class client{
	
	public static void main(String [] args){

		System.out.println("Enter Size of Array: ");
		
		Scanner sc=new Scanner(System.in);
		int size=sc.nextInt();
		int arr[]= new int[size];
		
		System.out.println("Enter Array elements:");
		for(int i=0;i<arr.length;i++){

			arr[i]=sc.nextInt();
		}
		
		Solution obj=new Solution();
		int val=obj.firstRepeated(arr,arr.length);

		System.out.println(val);
	}
}
