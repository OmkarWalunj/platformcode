
import java.util.*;

class Solution {
    public int singleNumber(int[] nums) {

        Arrays.sort(nums);

        for(int i=0;i<nums.length-1;i++){
            if(nums[i]!=nums[i+1]){
                return nums[i];
            }else{
                i++;
            }
        }
        return nums[nums.length-1];
        
    }
}

class client{
	
	public static void main(String [] args){

		System.out.println("Enter Size of Array: ");
		
		Scanner sc=new Scanner(System.in);
		int size=sc.nextInt();
		int arr[]= new int[size];
		
		System.out.println("Enter Array elements:");
		for(int i=0;i<arr.length;i++){

			arr[i]=sc.nextInt();
		}
		
		Solution obj=new Solution();
		int val=obj.singleNumber(arr);

		System.out.println(val);
	}
}

